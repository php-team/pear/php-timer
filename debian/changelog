php-timer (8.0.0-1) experimental; urgency=medium

  * Upload new major to experimental

  [ Sebastian Bergmann ]
  * Bump copyright year
  * Prepare release

  [ David Prévot ]
  * Update copyright (years)

 -- David Prévot <taffit@debian.org>  Fri, 07 Feb 2025 19:34:54 +0100

php-timer (7.0.1-3) unstable; urgency=medium

  * Source-only upload for testing migration

 -- David Prévot <taffit@debian.org>  Fri, 10 Jan 2025 12:06:00 +0100

php-timer (7.0.1-2) unstable; urgency=medium

  * Upload to unstable in sync with PHPUnit 11

 -- David Prévot <taffit@debian.org>  Fri, 10 Jan 2025 10:03:06 +0100

php-timer (7.0.1-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Use PHPStan instead of Psalm
  * Prepare release

  [ David Prévot ]
  * Update Standards-Version to 4.7.0

 -- David Prévot <taffit@debian.org>  Sun, 14 Jul 2024 10:51:16 +0200

php-timer (7.0.0-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Start development of next major version
  * Bump copyright year
  * Prepare release

  [ David Prévot ]
  * overrides/php-timer: Drop empty line
  * Update copyright (years)

 -- David Prévot <taffit@debian.org>  Sun, 04 Feb 2024 13:24:31 +0100

php-timer (6.0.0-1) experimental; urgency=medium

  * Upload new major version to experimental

  [ Remi Collet ]
  * Fix #34 test failing on 32-bit

  [ Sebastian Bergmann ]
  * Drop support for PHP 7.3 and PHP 7.4
  * Drop support for PHP 8.0
  * Bump copyright year
  * Add Security Policy
  * Prepare release

  [ David Prévot ]
  * Fix debian/gbp.conf
  * Set upstream metadata fields: Security-Contact.
  * Update standards version to 4.6.2, no changes needed.
  * Ship security notice
  * Update copyright (years and license)
  * Use phpunit 10 for tests

 -- David Prévot <taffit@debian.org>  Sat, 04 Feb 2023 00:23:54 +0100

php-timer (5.0.3-3) unstable; urgency=medium

  * Simplify gbp import-orig (and check signature)
  * Use dh-sequence-phpcomposer instead of --with phpcomposer
  * Install /u/s/pkg-php-tools/{autoloaders,overrides} files
  * Mark package as Multi-Arch: foreign
  * Update Standards-Version to 4.6.1

 -- David Prévot <taffit@debian.org>  Sun, 03 Jul 2022 13:41:54 +0200

php-timer (5.0.3-2) unstable; urgency=medium

  [ Remi Collet ]
  * Fix test failing on 32-bit (Closes: #966412)

 -- David Prévot <taffit@debian.org>  Mon, 21 Dec 2020 09:04:01 -0400

php-timer (5.0.3-1) unstable; urgency=medium

  * Source upload now that everything is in place

 -- David Prévot <taffit@debian.org>  Sun, 20 Dec 2020 17:25:51 -0400

php-timer (5.0.3-1~stage1) unstable; urgency=medium

  * Upload to unstable in sync with PHPUnit 9

  [ Sebastian Bergmann ]
  * Prepare release

  [ David Prévot ]
  * Update watch file format version to 4.
  * Update Standards-Version to 4.5.1

 -- David Prévot <taffit@debian.org>  Sun, 20 Dec 2020 15:32:59 -0400

php-timer (5.0.2-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Prepare release

  [ David Prévot ]
  * Rename main branch to debian/latest (DEP-14)
  * Set Rules-Requires-Root: no.

 -- David Prévot <taffit@debian.org>  Wed, 30 Sep 2020 11:44:55 -0400

php-timer (5.0.1-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Support PHP 8 for https://github.com/sebastianbergmann/phpunit/issues/4325
  * Prepare release

 -- David Prévot <taffit@debian.org>  Sun, 28 Jun 2020 13:51:50 -1000

php-timer (5.0.0-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Closes #31

 -- David Prévot <taffit@debian.org>  Wed, 10 Jun 2020 15:20:09 -1000

php-timer (4.0.0-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Ignore tests etc. from archive exports
  * Prepare release

  [ David Prévot ]
  * Document gbp import-ref usage
  * Use debhelper-compat 13
  * Simplify override_dh_auto_test
  * Build-Depend on recent phpunit

 -- David Prévot <taffit@debian.org>  Fri, 05 Jun 2020 17:50:12 -1000

php-timer (3.1.4-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Prepare release

 -- David Prévot <taffit@debian.org>  Sun, 26 Apr 2020 08:33:12 -1000

php-timer (3.0.0-1) experimental; urgency=medium

  * Upload version compatible with PHPUnit 9 to experimental

  [ Sebastian Bergmann ]
  * Bump copyright year
  * Drop support for PHP 7.1 and PHP 7.2
  * Prepare release

  [ David Prévot ]
  * debian/copyright: Update copyright (years)
  * debian/upstream/metadata:
    + Set fields: Bug-Database, Bug-Submit, Repository, Repository-Browse
    + Remove obsolete fields: Contact, Name
  * debian/control:
    + Drop versioned dependency satisfied in (old)stable
    + Update standards version to 4.5.0

 -- David Prévot <taffit@debian.org>  Fri, 07 Feb 2020 19:57:34 -1000

php-timer (2.1.2-2) unstable; urgency=medium

  * Upload to unstable now thet buster has been released
  * Update standards version, no changes needed.
  * Set upstream metadata fields: Contact, Name.
  * Add self to Uploaders
  * Extend d/clean for PHPUnit 8

 -- David Prévot <taffit@debian.org>  Sun, 18 Aug 2019 08:24:16 -1000

php-timer (2.1.2-1) experimental; urgency=medium

  * Team upload to experimental during the freeze

 -- David Prévot <taffit@debian.org>  Mon, 17 Jun 2019 09:33:00 -1000

php-timer (2.1.1-1) unstable; urgency=medium

  * Team upload

  [ Sebastian Bergmann ]
  * Bump copyright year
  * Prepare release

  [ David Prévot ]
  * Update copyright (years)
  * Use debhelper-compat 12
  * Drop get-orig-source target
  * Update Standards-Version to 4.3.0
  * Let the local package take precedence for tests

 -- David Prévot <taffit@debian.org>  Mon, 25 Feb 2019 12:52:31 -1000

php-timer (2.0.0-2) unstable; urgency=medium

  * Team upload, to unstable, with the rest of the latest PHPUnit stack
  * Delete autoload.php on clean
  * Drop Luis Uribe from Uploaders (Closes: #894005)

 -- David Prévot <taffit@debian.org>  Thu, 29 Mar 2018 10:40:36 -1000

php-timer (2.0.0-1) experimental; urgency=medium

  * Team upload to experimental since phpunit/sid currently depends on
    php-timer (<< 2~~)

  [ Sebastian Bergmann ]
  * Prepare release

  [ David Prévot ]
  * Update copyright (years)
  * Change installation path to track namespace
  * Build-Depend on ant
  * Move project repository to salsa.d.o
  * Update Standards-Version to 4.1.3

 -- David Prévot <taffit@debian.org>  Sun, 04 Mar 2018 18:05:20 -1000

php-timer (1.0.9-1) unstable; urgency=medium

  * Team upload
  * Adapt test calls to now provided phpunit.xml
  * Drop Thomas Goirand from uploaders as he requested
  * Update Standards-Version to 4.1.0

 -- David Prévot <taffit@debian.org>  Tue, 22 Aug 2017 12:19:24 -1000

php-timer (1.0.8-1) unstable; urgency=medium

  * Team upload

  [ Michael Moravec ]
  * Megabytes should be interpreted as MB, not Mb

  [ David Prévot ]
  * Update Standards-Version to 3.9.8

 -- David Prévot <taffit@debian.org>  Fri, 13 May 2016 20:52:11 -0400

php-timer (1.0.7-2) unstable; urgency=medium

  * Team upload
  * Update Standards-Version to 3.9.7
  * Rebuild with recent pkg-php-tools for the PHP 7.0 transition

 -- David Prévot <taffit@debian.org>  Sun, 20 Mar 2016 11:52:00 -0400

php-timer (1.0.7-1) unstable; urgency=medium

  * Team upload

  [ Sebastian Bergmann ]
  * Fix CS/WS issues

 -- David Prévot <taffit@debian.org>  Sun, 25 Oct 2015 22:06:54 -0400

php-timer (1.0.6-1) unstable; urgency=medium

  * Team upload

  [ Henrique Moody ]
  * Update license and copyright in all files

  [ David Prévot ]
  * Convert packaging to Composer source
  * Update copyright
  * Bump standards version to 3.9.6

 -- David Prévot <taffit@debian.org>  Mon, 22 Jun 2015 20:55:48 -0400

php-timer (1.0.5-1) unstable; urgency=medium

  * Update d/watch to point to pear.phpunit.de
  * Imported Upstream version 1.0.5
  * Build-depend on pear-channels
  * Bump debhelper to 9
  * Bump Standard-Version to 3.9.5
  * Change Build-depends on pkg-php-tools (>= 1.1~)
  * Add myself as uploader
  * Add Build-Depends on php5-cli (>= 5.3.7-1~) (Closes: #693681)
  * Update copyright year
  * Use canonical URI in Vcs-* fields

 -- Prach Pongpanich <prachpub@gmail.com>  Wed, 05 Feb 2014 15:46:20 +0700

php-timer (1.0.2-2) unstable; urgency=low

  * Removed useless channel.xml hack (Closes: #671955).
  * Added myself as uploader.
  * Removes LICENSE file from package.xml.

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 May 2012 14:18:21 +0000

php-timer (1.0.2-1) unstable; urgency=low

    * Initial release (Closes: #611767)

 -- Luis Uribe <acme@eviled.org>  Tue, 13 Mar 2012 22:10:30 -0500
